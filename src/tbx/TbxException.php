<?php
/**
 * TBX Exception wrapper
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\TBX;

class TbxException extends \Exception
{
	
}