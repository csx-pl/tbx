<?php
/**
 * TBX Translation
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\TBX;

use ArteQ\Utils\Text;

class Translation
{
	private $id;
	private $translation;
	private $lang;
	private $description;
	private $descriptionType;
	private $note;
	private $noteType;

	/* ====================================================================== */
	
	/**
	 * Create new Translation entry
	 * 
	 * @param string $translation
	 * @param string $lang
	 * @param string $id
	 */ 
	public function __construct($translation, $lang, $id = null)
	{
		if (empty($translation))
			throw new TbxException("Missing translation");
		$this->translation = $translation;

		if (empty($lang))
			throw new TbxException("Missing language code");
		$this->lang = $lang;

		// generate id if not provided
		if (empty($id))
			$id = Text::uuid();
		$this->id = $id;
	}

	/* ====================================================================== */
	
	/**
	 * Set optional translation note
	 * 
	 * @param string $note
	 * @param string $noteType
	 */ 
	public function setNote($note, $noteType)
	{
		$this->note = $note;
		$this->noteType = $noteType;
	}

	/* ====================================================================== */
	
	/**
	 * Set optional translation description
	 * 
	 * @param string $description
	 * @param string $type
	 */ 
	public function setDescription($description, $descriptionType = 'definition')
	{
		$this->description = $description;
		$this->descriptionType = $descriptionType;
	}

	/* ====================================================================== */
	
	/**
	 * Return XML string representation of Translation entry
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function generate(\DOMDocument $dom)
	{
		$langSet = $dom->createElement('langSet');
		$langSet->setAttribute('xml:lang', $this->lang);

		if (!empty($this->description))
		{
			$descripGrp = $dom->createElement('descripGrp');

			$descrip = $dom->createElement('descrip', $this->description);
			$descrip->setAttribute('type', $this->descriptionType);

			$descripGrp->appendChild($descrip);
			$langSet->appendChild($descripGrp);
		}

		$ntig = $dom->createElement('ntig');

		$termGrp = $dom->createElement('termGrp');
		
		$term = $dom->createElement('term', $this->translation);
		$term->setAttribute('id', $this->id);
		$termGrp->appendChild($term);

		if (!empty($this->note))
		{
			$termNote = $dom->createElement('termNote', $this->note);
			$termNote->setAttribute('type', $this->noteType);

			$termGrp->appendChild($termNote);
		}

		$ntig->appendChild($termGrp);
		$langSet->appendChild($ntig);

		return $langSet;
	}
}