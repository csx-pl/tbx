<?php
/**
 * TBX Writer
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\TBX;

class Writer
{
	const XML_VERSION = "1.0";
	const XML_ENCODING = "UTF-8";

	/**
	 * Root element TBX attributes
	 * @var array
	 */ 
	private $attributes = [
		"type" => "TBX",
		"xml:lang" => "en-EN",
	];

	/**
	 * TBX object
	 * @var \DOMDocument
	 */ 
	private $xml;

	/**
	 * TBX header title (opt)
	 * @var string
	 */ 
	private $title;

	/**
	 * TBX header description (opt)
	 * @var string
	 */
	private $source;

	/**
	 * List of Term
	 * @var array
	 */ 
	protected $terms = [];

	/* ====================================================================== */

	/**
	 * Create new TBX document
	 * 
	 * @param array $attributes
	 */ 	
	public function __construct($attributes = [])
	{
		$this->attributes = array_merge($this->attributes, $attributes);
		
		$xml = new \DOMDocument(self::XML_VERSION, self::XML_ENCODING);
		$xml->formatOutput = true;

		$this->xml = $xml;
	}

	/* ====================================================================== */

	/**
	 * Add title for XML header 
	 * 
	 * @param string $title
	 */ 
	public function addTitle($title)
	{
		$this->title = $title;
	}

	/* ====================================================================== */
	
	/**
	 * Add source for XML header 
	 * 
	 * @param string $source
	 */
	public function addSource($source)
	{
		$this->source = $source;
	}

	/* ====================================================================== */
	
	/**
	 * Add Term element
	 * 
	 * @param Term $term
	 */ 
	public function addTerm(Term $term)
	{
		$this->terms[] = $term;
	}

	/* ====================================================================== */

	/**
	 * Return XML string representation of TBX
	 * 
	 * @return string
	 */ 
	public function generate()
	{
		// root element
		$martif = $this->xml->createElement('martif');
		foreach ($this->attributes as $key => $value)
		{
			$martif->setAttribute($key, $value);
		}
		$this->xml->appendChild($martif);

		// header
		$header = $this->getHeader();
		$martif->appendChild($header);

		// terms
		$text = $this->xml->createElement('text');
		$body = $this->xml->createElement('body');

		foreach ($this->terms as $term)
		{
			$body->appendChild( $term->generate($this->xml) );
		}

		$text->appendChild($body);
		$martif->appendChild($text);

		return $this->xml->saveXML();
	}

	/* ====================================================================== */
	
	/**
	 * Create TBX header
	 * 
	 * @return \DOMElement
	 */ 
	private function getHeader()
	{
		$martifHeader = $this->xml->createElement('martifHeader');
		
		$fileDesc = $this->xml->createElement('fileDesc');

		if (!empty($this->title))
		{
			$titleStmt = $this->xml->createElement('titleStmt');
			
			$title = $this->xml->createElement('title', $this->title);
			$titleStmt->appendChild($title);
			$fileDesc->appendChild($titleStmt);
		}

		if (!empty($this->source))
		{
			$sourceDesc = $this->xml->createElement('sourceDesc');
			
			$p = $this->xml->createElement('p', $this->source);
			$sourceDesc->appendChild($p);
			$fileDesc->appendChild($sourceDesc);
		}

		$martifHeader->appendChild($fileDesc);

		return $martifHeader;
	}
}