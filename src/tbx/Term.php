<?php
/**
 * TBX Term
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\TBX;

use ArteQ\Utils\Text;

class Term
{
	private $id;
	private $note;
	private $translations = [];

	/* ====================================================================== */
	
	/**
	 * Create new Term entry
	 * 
	 * @param Translation $translation (opt)
	 * @param string $id (opt)
	 */ 
	public function __construct(Translation $translation = null, $id = null)
	{
		// generate id if not provided
		if (empty($id))
			$id = Text::uuid();
		$this->id = $id;

		if (!empty($translation))
			$this->addTranslation($translation);		
	}

	/* ====================================================================== */
	
	/**
	 * Return XML string representation of Term entry
	 * 
	 * @param \DOMDocument $dom
	 * @return \DOMElement
	 */ 
	public function generate(\DOMDocument $dom)
	{
		$termEntry = $dom->createElement('termEntry');
		$termEntry->setAttribute('id', $this->id);

		if (!empty($this->note))
		{
			$note = $dom->createElement('note', $this->note);
			$termEntry->appendChild($note);
		}

		foreach ($this->translations as $translation)
		{
			$termEntry->appendChild( $translation->generate($dom) );
		}
		
		return $termEntry;
	}

	/* ====================================================================== */
	
	/**
	 * Add new Translation unit to Term
	 * 
	 * @param Translation $translation
	 */ 
	public function addTranslation(Translation $translation)
	{
		$this->translations[] = $translation;
	}

	/* ====================================================================== */
	
	/**
	 * Set optional term note
	 * 
	 * @param string $note
	 */ 
	public function setNote($note)
	{
		$this->note = $note;
	}
}