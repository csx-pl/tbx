<?php
/**
 * TBX generator tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\TBX\Writer;
use ArteQ\TBX\Term;
use ArteQ\TBX\Translation;

class WriterTest extends TestCase
{
	private $writer;

	/* ====================================================================== */
	
	public function setUp()
	{
		$writer = new Writer();
		$this->writer = $writer;
	}

	/* ====================================================================== */
	
	public function testCanAddTermWithTranslations()
	{
		$translation1 = new Translation('foo 1', 'pl-PL', '111');
		$translation2 = new Translation('foo 2', 'en-EN', '222');
		
		$term = new Term();
		$term->addTranslation($translation1);
		$term->addTranslation($translation2);

		$this->writer->addTerm($term);
		$xml = $this->writer->generate();

		$this->assertContains('<langSet xml:lang="pl-PL">', $xml);
		$this->assertContains('<term id="111">foo 1</term>', $xml);
		$this->assertContains('<langSet xml:lang="en-EN">', $xml);
		$this->assertContains('<term id="222">foo 2</term>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanAddTitle()
	{
		$this->writer->addTitle('foo bar');
		$xml = $this->writer->generate();

		$this->assertContains('<martifHeader>', $xml);
		$this->assertContains('<fileDesc>', $xml);
		$this->assertContains('<titleStmt>', $xml);
		$this->assertContains('<title>foo bar</title>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanAddSource()
	{
		$this->writer->addSource('foo bar');
		$xml = $this->writer->generate();
		
		$this->assertContains('<martifHeader>', $xml);
		$this->assertContains('<fileDesc>', $xml);
		$this->assertContains('<sourceDesc>', $xml);
		$this->assertContains('<p>foo bar</p>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanGetXml()
	{
		$xml = $this->writer->generate();
		
		$this->assertContains('<martif type="TBX" xml:lang', $xml);
	}
}