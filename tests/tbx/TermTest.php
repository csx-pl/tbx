<?php
/**
 * TBX Term unit tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\TBX\Term;
use ArteQ\TBX\Translation;
use ArteQ\TBX\TbxException;

class TermTest extends TestCase
{
	private $dom;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dom = new \DOMDocument("1.0", "UTF-8");
		$dom->formatOutput = true;
		$this->dom = $dom;
	}

	/* ====================================================================== */
	
	public function testCanCreateEmptyTerm()
	{
		$term = new Term();

		$termEntry = $term->generate($this->dom);
		$this->dom->appendChild($termEntry);
		$xml = $this->dom->saveXML();

		$this->assertContains('<termEntry id="', $xml);
	}

	/* ====================================================================== */
	
	public function testCanCreateTermWithTranslation()
	{
		$translation = new Translation('foo bar', 'pl-PL', '111');
		$term = new Term($translation, '123-456');

		$termEntry = $term->generate($this->dom);
		$this->dom->appendChild($termEntry);
		$xml = $this->dom->saveXML();

		$this->assertContains('<termEntry id="123-456">', $xml);
		$this->assertContains('<term id="111">foo bar</term>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanAddMultipleTranslations()
	{
		$translation1 = new Translation('foo 1', 'pl-PL', '111');
		$translation2 = new Translation('foo 2', 'en-EN', '222');
		
		$term = new Term();
		$term->addTranslation($translation1);
		$term->addTranslation($translation2);

		$termEntry = $term->generate($this->dom);
		$this->dom->appendChild($termEntry);
		$xml = $this->dom->saveXML();

		$this->assertContains('<langSet xml:lang="pl-PL">', $xml);
		$this->assertContains('<term id="111">foo 1</term>', $xml);
		$this->assertContains('<langSet xml:lang="en-EN">', $xml);
		$this->assertContains('<term id="222">foo 2</term>', $xml);
	}
}
