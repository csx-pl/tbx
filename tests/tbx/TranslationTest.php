<?php
/**
 * TBX Translation unit tests
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\TBX\Translation;
use ArteQ\TBX\TbxException;

class TranslationTest extends TestCase
{
	private $dom;

	/* ====================================================================== */
	
	public function setUp()
	{
		$dom = new \DOMDocument("1.0", "UTF-8");
		$dom->formatOutput = true;
		$this->dom = $dom;
	}

	/* ====================================================================== */
	
	public function testCanCreateTranslation()
	{
		$translation = new Translation('foo bar', 'pl-PL');

		$langSet = $translation->generate($this->dom);
		$this->dom->appendChild($langSet);
		$xml = $this->dom->saveXML();

		$this->assertContains('<langSet xml:lang="pl-PL">', $xml);
		$this->assertContains('<ntig>', $xml);
		$this->assertContains('<termGrp>', $xml);
		$this->assertContains('<term id=', $xml);
	}

	/* ====================================================================== */
	
	public function testCantCreateTranslationWithoutTerm()
	{
		$this->expectException(TbxException::class);
		$translation = new Translation(null, null);
	}

	/* ====================================================================== */
	
	public function testCantCreateTranslationWithoutLang()
	{
		$this->expectException(TbxException::class);
		$translation = new Translation('foo bar', null);
	}

	/* ====================================================================== */
	
	public function testCanCreateTranslationWithId()
	{
		$translation = new Translation('foo bar', 'pl-PL', '123-456');

		$langSet = $translation->generate($this->dom);
		$this->dom->appendChild($langSet);
		$xml = $this->dom->saveXML();

		$this->assertContains('<term id="123-456">foo bar</term>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanCreateTranslationWithDescription()
	{
		$translation = new Translation('foo bar', 'pl-PL');
		$translation->setDescription('xyz', 'type');

		$langSet = $translation->generate($this->dom);
		$this->dom->appendChild($langSet);
		$xml = $this->dom->saveXML();

		$this->assertContains('<descripGrp>', $xml);
		$this->assertContains('<descrip type="type">xyz</descrip>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanCreateTranslationWithDescriptionDefault()
	{
		$translation = new Translation('foo bar', 'pl-PL');
		$translation->setDescription('xyz');

		$langSet = $translation->generate($this->dom);
		$this->dom->appendChild($langSet);
		$xml = $this->dom->saveXML();

		$this->assertContains('<descripGrp>', $xml);
		$this->assertContains('<descrip type="definition">xyz</descrip>', $xml);
	}

	/* ====================================================================== */
	
	public function testCanCreateTranslationWithNote()
	{
		$translation = new Translation('foo bar', 'pl-PL');
		$translation->setNote('note', 'type');

		$langSet = $translation->generate($this->dom);
		$this->dom->appendChild($langSet);
		$xml = $this->dom->saveXML();

		$this->assertContains('<termNote type="type">note</termNote>', $xml);
	}
}